import numpy as np

from ConvPY.Blobs.Blob import Blob
from ConvPY.Layers.PlaceHolderLayer import PlaceHolderLayer
from ConvPY.Layers.InnerProductLayer import InnerProductLayer
from ConvPY.Layers.Losses.EuclideanLossLayer import EuclideanLossLayer
from ConvPY.Net.Net import Net
from ConvPY.Solvers.SGD import SGDSolver

x = PlaceHolderLayer('x',(32,64))
y = PlaceHolderLayer('y',(32,5))

fc1 = InnerProductLayer(x,10,name='fc1')
fc2 = InnerProductLayer(fc1,5,name='fc2')


loss = EuclideanLossLayer(fc2,y)

data = np.random.random((32,64))
label = np.random.random((32,5))
feed_dict={'x':Blob(data),'y':Blob(label)}

net = Net()
solver = SGDSolver(learn_rate=0.001)
net.SetSolver(solver)

z = net.Push_Forward(targetlayer=loss,feed_dict=feed_dict)
net.Back_Propagation(loss)

fc2_weight1 = fc2.weight.data
fc2_diff1 = fc2.weight.diff

net.Step()
net.ZeroGard()

fc2_weight2 = fc2.weight.data
