from ConvPY import *

def test1():
    graphs = Graphs()

    x = PlaceHolder('x')
    y = PlaceHolder('y')
    z = PlaceHolder('z')

    t1 = Add(x,y)
    t2 = Div(t1,z)

    print(id(x),id(y),id(z),id(t1),id(t2))
    print(type(x))

    ans = graphs.Push_Forward(t2,feed_dict={'x':2,'y':3,'z':7})

    print('ans',ans)

def test2():

    graphs = Graphs()

    x = PlaceHolder('x')
    x2 = Mul(x,x)

    graphs.Push_Forward(x2,feed_dict={'x':1.2})

def test3():

    from ConvPY import *

    graphs = Graphs()

    a = Parameter(3)
    b = Parameter(5)
    c = PlaceHolder('c')

    z = Add(a,b)

    z2 = Mul(z,c)

    graphs.Push_Forward(z2,feed_dict={'c':9})

    graphs.Back_Propagation(z2)

    a.diff
    b.diff
    c.diff
