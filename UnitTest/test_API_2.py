import numpy as np

from ConvPY.Layers import *
from ConvPY.Blobs import Blob

a = np.arange(1,15+1).reshape(3,5)
a = Blob(a)

b = InnerProductLayer(a, 4, usebaise=True).forward()

b2 = InnerProductLayer(b, 3)

a2 = b.forward()

c = np.arange(1,12+1).reshape(3,4)

b.backward(c)

print(b.diff_weight)
print(b.diff_baise)
print(b.diff)
