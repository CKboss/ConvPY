import numpy as np

from sklearn import datasets
from sklearn.preprocessing import OneHotEncoder

from ConvPY.Blobs.Blob import Blob
from ConvPY.Layers.PlaceHolderLayer import PlaceHolderLayer
from ConvPY.Layers.InnerProductLayer import InnerProductLayer
from ConvPY.Layers.Losses.EuclideanLossLayer import EuclideanLossLayer
from ConvPY.Layers.Losses.CrossEntropyLossLayer import CrossEntropyLossLayer
from ConvPY.Layers.SigmoidLayer import SigmoidLayer
from ConvPY.Layers.SoftmaxLayer import SoftmaxLayer
from ConvPY.Net.Net import Net
from ConvPY.Solvers.SGD import SGDSolver

# load data
iris = datasets.load_iris()
X = iris.data[:,]
y1 = iris.target
Y = np.zeros((150,3))
for i in range(len(y1)): Y[i,y1[i]] = 1


x = PlaceHolderLayer('x',(150,4))
y = PlaceHolderLayer('y',(150,3))

fc1 = InnerProductLayer(x,4,name='fc1')
fc2 = InnerProductLayer(fc1,4,name='fc2')
fc3 = InnerProductLayer(fc1,3,name='fc3')
sof = SoftmaxLayer(fc3,name='softmax_layer')
loss = CrossEntropyLossLayer(sof,y)

X2 = ( X-np.mean(X) ) / np.var(X)
feed_dict={'x':Blob(X2),'y':Blob(Y)}

net = Net()
lr = 0.0001
solver = SGDSolver(learn_rate=lr)
net.SetSolver(solver)

for i in range(2000):
    net.ZeroGard()
    z = net.Push_Forward(targetlayer=loss,feed_dict=feed_dict)
    if i % 50 == 0:
        print('i:',i,' loss:',z.data)
    if i!=0 and i%1000 == 0:
        lr = lr*0.6
        solver = SGDSolver(learn_rate=lr)
        net.SetSolver(solver)

    net.Back_Propagation(loss)
    net.Step()


t = net.Push_Forward(targetlayer=sof,feed_dict=feed_dict)
np.argmax(t.data,1)

