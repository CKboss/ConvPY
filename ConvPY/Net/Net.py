import numpy as np

from ConvPY.Layers import *
from ConvPY.Blobs.Blob import Blob

LAYERS_KIND = {

    BaseLayer:100,
    PlaceHolderLayer:101,
    InnerProductLayer:102,
    SigmoidLayer:103,
    SoftmaxLayer:104,

    # loss
    EuclideanLossLayer:201,
    CrossEntropyLossLayer:202,
}

class Net(object):

    def __init__(self):
        self.feed_dict = None
        self.params = []

    def Built_ToPo(self,x):
        pass

    def ZeroGard(self):
        for param in self.params:
            param.zero_diff()

    def SetSolver(self,solver):
        self.optimize = solver

    def Step(self):
        for param in self.params:
            self.optimize.optim(param)

    def dfs_FW(self,x):
        layer_kind = LAYERS_KIND.get(type(x),-1)
        if layer_kind == -1:
            raise NotImplementedError('{} layer is not find !'.format(type(x)))
        elif layer_kind == 101:
            # try to find value from feed dict
            try:
                return [self.feed_dict[x.name]]
            except:
                raise AttributeError('{} is not find in feed_dict !'.format(x.name))

        elif layer_kind == 102:
            # InnerProduct Layer
            self.params += x.blobs_
            ret = self.dfs_FW(x.bottom[0])
            return [x.forward(ret)]
        elif layer_kind in [103,104]:
            # 103 sigmoid layer
            # 104 softmax layer
            ret = self.dfs_FW(x.bottom[0])
            return [x.forward(ret)]
        elif layer_kind in [201,202]:
            # 201 EuclideanLossLayer
            # 202 CrossEntropyLossLayer
            ret = [self.dfs_FW(x.bottom[0])[0],self.dfs_FW(x.bottom[1])[0]]
            # ret[0] is predict
            # ret[1] is real label
            return [x.forward(ret)]
        else:
            raise NotImplementedError('Forward of layer {} is not implemented'.format(type(x)))

    def dfs_BW(self,x: BaseLayer,delta: Blob = None):

        # print('during BW:',x.name)
        # if delta is not None:
        #     print('delta shape:',delta.getShape())

        layer_kind = LAYERS_KIND.get(type(x),-1)
        if layer_kind == 100:
            raise NotImplementedError('{} layer is not find !'.format(type(x)))
        elif layer_kind == 101:
            return
        elif layer_kind in [102,103,104,201,202]:
            diff = x.backward(delta=delta)
            for bottom in x.bottom:
                self.dfs_BW(bottom,diff)
        else:
            raise NotImplementedError('Backward of layer {} is not implemented'.format(type(x)))

    def Push_Forward(self, targetlayer: BaseLayer, feed_dict: dict=None):
        self.feed_dict = feed_dict
        ret = self.dfs_FW(targetlayer)
        if len(ret) > 1:
            return len(ret), ret
        return ret[0]

    def Back_Propagation(self, sinklayer: BaseLayer,delta: Blob = None):
        return self.dfs_BW(sinklayer)
