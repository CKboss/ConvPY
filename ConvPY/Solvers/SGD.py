from ConvPY.Blobs.Blob import Blob
from ConvPY.Solvers.BaseSolver import BaseSolver

class SGDSolver(BaseSolver):
    def __init__(self,learn_rate=0.1,name='sgd_solver'):
        super(SGDSolver,self).__init__()
        self.name = name
        self.lr = learn_rate
    def optim(self,x: Blob):
        x.data = x.data-self.lr*x.diff
