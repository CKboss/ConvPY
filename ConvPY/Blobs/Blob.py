from functools import reduce

import numpy as np

class Blob(object):

    def __init__(self,value: np.ndarray,need_gard=True):
        self.data = value
        self.diff = np.zeros_like(self.data)
        self.rank = len(self.data.shape)
        self.need_gard = need_gard

    def getShape(self):
        return self.data.shape

    def count(self):
        return reduce(lambda x,y: x*y,self.data.shape)

    def zero_diff(self):
        self.diff = np.zeros_like(self.data)
