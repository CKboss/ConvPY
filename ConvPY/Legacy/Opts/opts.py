
class BaseOPT(object):

    def __init__(self):

        self.bottom = []
        self.top = []

        self.need_grad = False

        self.value = None
        self.diff = []
        self.name = 'BaseOPT'

    def __add__(self, other):
        return self.value + other.value

    def __sub__(self, other):
        return self.value - other.value

    def __mul__(self, other):
        return self.value * other.value

    def __truediv__(self, other):
        return self.__floordiv__(other)
    def __floordiv__(self, other):
        return self.value / other.value

    def forward(self):
        raise NotImplementedError

    def backward(self,delta=1):
        raise NotImplementedError

    def getDiff(self):
        return sum(self.diff)

    def __repr__(self):
        line1 = 'id: {} name: {}\n'.format(id(self),self.name)
        line2 = 'value: {} diff: {}\n'.format(self.value,self.diff)
        line3 = 'num_of_bottom: {} num_of_top: {}'.format(len(self.bottom),len(self.top))
        return line1+line2+line3

class PlaceHolder(BaseOPT):
    def __init__(self,name:str):
        super(PlaceHolder,self).__init__()
        self.name = name

class Parameter(BaseOPT):
    def __init__(self,value):
        super(Parameter,self).__init__()
        self.value = float(value)

class Add(BaseOPT):

    def __init__(self,x:BaseOPT,y :BaseOPT):

        super(Add,self).__init__()

        self.arg0 = x
        self.arg1 = y

        self.bottom = [self.arg0,self.arg1]
        if self not in self.arg0.top: self.arg1.top.append(self)
        if self not in self.arg0.top: self.arg1.top.append(self)

    def forward(self):
        return self.arg0+self.arg1

    def backward(self,delta=1):
        # diff arg0 and diff arg1
        self.diff = [1.0,1.0]
        self.diff = list(map(lambda x: x*delta,self.diff))

class Sub(BaseOPT):

    def __init__(self,x:BaseOPT,y:BaseOPT):
        super(Sub,self).__init__()

        self.arg0 = x
        self.arg1 = y

        self.bottom = [self.arg0,self.arg1]
        if self not in self.arg0.top: self.arg0.top.append(self)
        if self not in self.arg1.top: self.arg1.top.append(self)

    def forward(self):
        return self.arg0-self.arg1

    def backward(self,delta=1):
        self.diff = [1,-1]
        self.diff = list(map(lambda x: x*delta,self.diff))

class Mul(BaseOPT):

    def __init__(self,x:BaseOPT,y:BaseOPT):

        super(Mul,self).__init__()

        self.arg0 = x
        self.arg1 = y

        self.bottom = [self.arg0,self.arg1]
        if self not in self.arg0.top: self.arg0.top.append(self)
        if self not in self.arg1.top: self.arg1.top.append(self)

    def forward(self):
        return self.arg0*self.arg1

    def backward(self,delta=1):
        self.diff = [self.arg1.value,self.arg0.value]
        self.diff = list(map(lambda x: x*delta,self.diff))

class Div(BaseOPT):

    def __init__(self,x:BaseOPT,y:BaseOPT):

        super(Div,self).__init__()

        self.arg0 = x
        self.arg1 = y

        self.bottom = [self.arg0,self.arg1]
        if self not in self.arg0.top: self.arg0.top.append(self)
        if self not in self.arg1.top: self.arg1.top.append(self)

    def forward(self):
        return self.arg0/self.arg1

    def backward(self,delta=1):
        self.diff = [1/self.arg1.value, -1 * self.arg0.value*self.arg1.value**-2]
        self.diff = list(map(lambda x: x*delta,self.diff))
