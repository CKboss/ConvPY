from ConvPY.Layers import *
from ConvPY.Legacy.Opts.opts import *

LAYER_KIND = {PlaceHolderLayer:100, InnerProductLayer:101}

OPT_KIND = {PlaceHolder:0,Parameter:1,Add:2,Sub:3,Mul:4,Div:5}

OPT_KIND.update(LAYER_KIND)

class Graphs(object):

    def __init__(self):
        self.opts = []
        self.params = []
        self.feed_dict = dict()

    def dfs_FW(self, node: BaseOPT):
        if node not in self.opts:
            self.opts.append(node)
        opt_kind = OPT_KIND.get(type(node),-1)
        if opt_kind == 0:
            ret = self.feed_dict.get(node.name,None)
            if ret is None:
                raise AttributeError('{} is not found in feed_dict.'.format(node.name))
            else:
                node.value = ret
                return ret
        elif opt_kind == 1:
            return node.value
        elif 2 <= opt_kind <= 5 :
            part_0 = self.dfs_FW(node.bottom[0])
            part_1 = self.dfs_FW(node.bottom[1])
            ret = node.forward()
            node.value = ret
            return ret
        else:
            raise NotImplementedError('opt {} is not implement.'.format(opt_kind))

    def dfs_BP(self,node: BaseOPT,delta):

        opt_kind = OPT_KIND.get(type(node),-1)
        node.diff.append(delta)
        if opt_kind == 0:
            return
        elif opt_kind == 1:
            if node not in self.params:
                self.params.append(node)
            return
        elif 2 <= opt_kind <= 5:
            node.backward(delta=delta)
            for i in range(len(node.bottom)):
                self.dfs_BP(node.bottom[i],node.diff[i])
        else:
            raise NotImplementedError('opt {} is not implement.'.format(opt_kind))

    def Push_Forward(self, node: BaseOPT, feed_dict: dict=None):
        self.feed_dict = feed_dict or dict()
        return self.dfs_FW(node)

    def Back_Propagation(self,node: BaseOPT):
        self.dfs_BP(node,delta=1)

    def zero_grad(self):
        for item in self.opts: item.diff = []
