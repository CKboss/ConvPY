import numpy as np

from ConvPY.Blobs import *
from ConvPY.Layers.BaseLayer import BaseLayer

class ReLULayer(BaseLayer):

    def __init__(self):
        super(ReLULayer,self).__init__()

    def forward(self,input_blobs: list = None):
        pass

    def backward(self,delta: Blob = None):
        pass
