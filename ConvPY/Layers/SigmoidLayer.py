import numpy as np

from ConvPY.Layers.BaseLayer import BaseLayer
from ConvPY.Blobs.Blob import Blob


def sigmoid(x: np.ndarray):
    return 1/(1+np.exp(x*-1))

class SigmoidLayer(BaseLayer):
    ''' SigmoidLayer
    forward: 1/(1+np.exp(x*-1))
    backward: sigmod(x)*(1-sigmod(x))
    '''

    def __init__(self,inputlayer,name='sigmoid_layer'):
        super(SigmoidLayer,self).__init__()
        self.name = name
        self.arg0 = inputlayer
        self.bottom.append(self.arg0)

    def forward(self,input_blobs: list = None):
        self.input_blob = input_blobs[0]
        return Blob(sigmoid(input_blobs[0].data))

    def backward(self,delta: Blob = None):
        x = self.input_blob
        if delta is None:
            delta = Blob(np.ones_like(x.data))
        return Blob(sigmoid(x.data)*(1-sigmoid(x.data))*delta.data)

if __name__=='__main__':
    a = np.array([[1,2,3],[4,5,6],[99,-1,-999]])
    print(sigmoid(a))
    print(sigmoid(a)*(1-sigmoid(a)))
