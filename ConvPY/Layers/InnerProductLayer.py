import numpy as np

from ConvPY.Layers.BaseLayer import BaseLayer
from ConvPY.Blobs import Blob

class InnerProductLayer(BaseLayer):
    '''fullconnectlayer to computer W*x+b'''

    def __init__(self,inputLayer: BaseLayer,outputsize: int,usebaise=False,name='InnerProducterLayer'):
        '''
        :param inputLayer: a Baselayer rank is 2 ( batchsize x n )
        :param outputsize: a Baselayer rank is 2 ( batchsize x outputsize )
        '''
        super(InnerProductLayer, self).__init__()

        self.name = name

        self.arg0 = inputLayer
        self.bottom.append(self.arg0)

        self.usebaise = usebaise
        self.ouputsize = outputsize

        b,n = inputLayer.blobs_[0].data.shape
        m = outputsize

        self.weight = Blob(np.random.normal(size=(n,m)))
        self.blobs_.append(self.weight)

        if self.usebaise:
            self.baise = Blob(np.zeros(m))
            self.blobs_.append(self.baise)


    def forward(self,input_blobs: list = None):
        ''' to computer: (bxn) @ (n,m) + (m)
        '''
        blob = input_blobs[0]
        self.input_blob = blob
        if self.usebaise:
            newvalue = np.dot(blob.data,self.weight.data) + self.baise.data
        else:
            newvalue = np.dot(blob.data,self.weight.data)
        return Blob(newvalue)

    def backward(self,delta: Blob=None):
        ''' calu diff of self.weight self.baise and top_diff
        diff_w = np.transpose(x) @ top_diff
        diff_b = topdiff @ np.ones(m,1)
        diff = todiff @ weight^T
        :param delta: the diff from top ( b x m ) or np.ones(b,m) if delta is none
        :return:
        '''
        # error from top
        b,n = self.input_blob.getShape()
        n,m = self.weight.getShape()

        # get weight and baise diff
        if delta is None:
            delta = Blob(np.ones(shape=(b,m)))

        # print('name:',self.name,' ')
        self.weight.diff += np.transpose(self.input_blob.data).dot(delta.data)

        if self.usebaise == True:
            self.baise.diff += np.dot(delta.data,np.ones(shape=(m,1)))

        # get top_diff to BP to nextlayer
        self.diff = Blob(np.dot(delta.data,np.transpose(self.weight.data)))
        return self.diff
