import numpy as np

from ConvPY.Blobs import Blob

class BaseLayer(object):

    def __init__(self):
        self.name = 'BaseLayer'
        # 图的拓扑结构
        self.bottom = []
        self.top = []
        # 具体的BLOB
        self.blobs_ = []


    def forward(self,input_blobs: list = None):
        # return a Blob
        raise NotImplementedError

    def backward(self,delta: Blob = None):
        # 计算每个参数的导数并把本层的误差传递到后面
        raise NotImplementedError

