import numpy as np

from ConvPY.Layers.BaseLayer import BaseLayer
from ConvPY.Blobs import Blob

class EuclideanLossLayer(BaseLayer):

    def __init__(self,inputlayer: BaseLayer,targetlayer):

        super(EuclideanLossLayer,self).__init__()

        self.arg0 = inputlayer
        self.arg1 = targetlayer
        self.bottom.append(self.arg0)
        self.bottom.append(self.arg1)
        self.blobs_ = [None,None]


    def forward(self,input_blobs: list = None):

        # (x-t)^2 / n
        x = input_blobs[0]
        t = input_blobs[1]

        self.blobs_[0] = x
        self.blobs_[1] = t

        assert x.getShape() == t.getShape(),'shape_of_x: {} shape_of_t: {}'.format(x.getShape(),t.getShape())

        c = np.sum(np.square(x.data-t.data),1)/(t.getShape()[0])
        y = Blob(np.average(c))

        return y

    def backward(self,delta: Blob = None):

        if delta is not None:
            raise NotImplementedError
        self.diff = 2* self.blobs_[0].data / self.blobs_[0].getShape()[1]
        self.diff = Blob(self.diff)
        return self.diff