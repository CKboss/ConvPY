import numpy as np

from ConvPY.Layers.BaseLayer import BaseLayer
from ConvPY.Blobs.Blob import Blob

class CrossEntropyLossLayer(BaseLayer):
    ''' CrossEntropyLossLayer
    P_n \in {0,1} probility of the predict
    P'_n is 0 or 1 a one hot label of real label
    $ loss = -1 * \sum_{n=1}^N [ P_n * log(P'_n) + (1-P_n) * log(1-P'_n) $
    '''
    def __init__(self,inputlayer: BaseLayer,targetlayer: BaseLayer):
        super(CrossEntropyLossLayer,self).__init__()

        self.arg0 = inputlayer
        self.arg1 = targetlayer
        self.bottom.append(self.arg0)
        self.bottom.append(self.arg1)
        self.blobs_ = [None,None]

    def forward(self,input_blobs: list = None):

        x = input_blobs[0]
        t = input_blobs[1]
        self.blobs_[0] = x
        self.blobs_[1] = t

        assert x.getShape() == t.getShape(),'shape_of_x: {} shape_of_t: {}'.format(x.getShape(),t.getShape())

        p = x.data
        l = t.data
        self.N = t.data.shape[1]
        c = np.average(np.average(p*np.log1p(l)+(1-p)*np.log1p(1-l),1)*-1/self.N)

        return Blob(c)

    def backward(self,delta: Blob = None):
        if delta is not None:
            raise NotImplementedError
        l = self.blobs_[1].data
        return Blob((np.log1p(l)-np.log1p(1-l))*(-1/self.N))


if __name__=='__main__':

    l = np.array([[1,0,0],[0,1,0]])
    p = np.array([[0.25,0.5,0.25],[0.05,0.7,0.25]])
    np.average( p*np.log1p(l)+(1-p)*np.log1p(1-l))

    np.log1p(l) - np.log1p(1-l)
    np.average(np.log1p(l) - np.log1p(1-l),1)
