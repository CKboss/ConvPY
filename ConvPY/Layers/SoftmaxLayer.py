import numpy as np

from ConvPY.Layers.BaseLayer import BaseLayer
from ConvPY.Blobs.Blob import Blob

class SoftmaxLayer(BaseLayer):

    def __init__(self,inputlayer,name='softmax_layer'):
        super(SoftmaxLayer,self).__init__()
        self.name = name
        self.arg0 = inputlayer
        self.bottom.append(self.arg0)

    def forward(self,input_blobs: list = None):
        self.input_blob = input_blobs[0]
        x = self.input_blob.data
        x_exp = np.exp(x)
        return Blob(np.transpose(np.transpose(x_exp)/np.sum(x_exp,1)))

    def backward(self,delta: Blob = None):

        x = self.input_blob.data

        if delta is None:
            delta = Blob(np.ones_like(x))
        ex = np.exp(x)
        sm = np.sum(ex,1).reshape(-1,1)

        return Blob((sm-ex)*ex/(sm*sm)*delta.data)

if __name__=='__main__':
    a = np.array([[1,2,3],[4,5,6],[99,-1,-999]])
    np.exp(a)
    np.sum(np.exp(a),1)
    np.transpose( np.transpose(np.exp(a)) / np.sum(np.exp(a),1) )
    b = np.array([3,6,1000])
    a.T/b
    np.exp(a)/(np.sum(np.exp(a),1))

    x = np.array([[1,2,3,4],[4,5,6,7],[99,-1,-99,9]])
    ex = np.exp(x)
    sm = np.sum(ex,1)
    sm = sm.reshape(-1,1)
    (sm-ex)*ex/(sm*sm)
