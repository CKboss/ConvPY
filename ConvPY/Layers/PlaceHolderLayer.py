import numpy as np

from ConvPY.Blobs.Blob import Blob
from ConvPY.Layers import *

class PlaceHolderLayer(BaseLayer):

    def __init__(self,name: str,shape: tuple):
        super(PlaceHolderLayer,self).__init__()
        self.name = name
        self.shape = shape
        self.blobs_.append(Blob(np.zeros(shape)))

    def forward(self,input_blobs: list = None):
        raise NotImplementedError
